import ButtonCDT from "../../components/forms/ButtonCDT";
import { useCookies } from "react-cookie";
import { Navbar } from "../../components/Navbar";
import { useDashboardData } from "../../api/hooks/useDashboardData";
import Loading from "../../components/Loading";
import { Fragment } from "react";
import Announcement from "../../components/Announcement";
import PageWrapper from "../../components/PageWrapper";
import ResourceTable from "../../components/ResourceTable";
export default function Dashboard() {
  const [cookies, setCookie, removeCookie] = useCookies(["token"]);
  const dashboardData = useDashboardData(cookies.token);

  if (!dashboardData.loading) {
    return (
      <Fragment>
        <Navbar
          data={dashboardData.loggedInfo[0]}
          removeCookie={removeCookie}
        />
        <PageWrapper>
          <Announcement announcement={dashboardData.announcement} />
          <ResourceTable data={dashboardData.resource} />
        </PageWrapper>
      </Fragment>
    );
  } else {
    return <Loading />;
  }
}
