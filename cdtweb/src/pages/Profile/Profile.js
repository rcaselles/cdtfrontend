import { useCookies } from "react-cookie";
import { Navbar } from "../../components/Navbar";
import { useDashboardData } from "../../api/hooks/useDashboardData";
import Loading from "../../components/Loading";
import { Fragment } from "react";
import PageWrapper from "../../components/PageWrapper";
import Taskbar from "../../components/Taskbar";
import { useState } from "react";
import Edit from "./Edit";
import Password from "./Password";

export default function Profile() {
  const [cookies, setCookie, removeCookie] = useCookies(["token"]);
  const dashboardData = useDashboardData(cookies.token);
  const [actual, setActual] = useState("edit");
  function ActualPage() {
    switch (actual) {
      case "edit":
        return <Edit />;
      case "password":
        return <Password />;
    }
  }
  if (!dashboardData.loading) {
    return (
      <Fragment>
        <Navbar
          data={dashboardData.loggedInfo[0]}
          removeCookie={removeCookie}
        />
        <Taskbar title={"Profile"} setPage={setActual} />
        <PageWrapper>
          <ActualPage />
        </PageWrapper>
      </Fragment>
    );
  } else {
    return <Loading />;
  }
}
