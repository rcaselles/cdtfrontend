import CenterWrapper from "../../components/CenterWrapper";
import SmallCard from "../../components/forms/SmallCard";
import SimpleForm from "../../components/forms/SimpleForm";
import passwordForm from "../../forms/static/passwordedit.json";
import { useForm } from "react-hook-form";
import updatePassword from "../../api/services/profile/password";
import { useCookies } from "react-cookie";

export default function Password() {
  const methods = useForm();
  const [cookies, setCookie, removeCookie] = useCookies(["token"]);

  return (
    <CenterWrapper>
      <SmallCard>
        <SimpleForm
          content={passwordForm}
          methods={methods}
          onSubmit={(data) => updatePassword(data, cookies.token)}
        />
      </SmallCard>
    </CenterWrapper>
  );
}
