import { Card } from "primereact/card";

export default function Edit() {
  return (
    <Card
      title="Simple Card"
      style={{ width: "50vw", marginBottom: "2em", marginLeft: "25vw" }}
    >
      <p className="m-0" style={{ lineHeight: "1.5" }}>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore sed
        consequuntur error repudiandae numquam deserunt quisquam repellat libero
        asperiores earum nam nobis, culpa ratione quam perferendis esse,
        cupiditate neque quas!
      </p>
    </Card>
  );
}
