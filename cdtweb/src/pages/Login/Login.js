import "./Login.css";
import { useForm } from "react-hook-form";
import CenterWrapper from "../../components/CenterWrapper";
import SmallCard from "../../components/forms/SmallCard";
import loginForm from "../../forms/static/login.json";
import SimpleForm from "../../components/forms/SimpleForm";
import LoginUser from "../../api/services/LoginService";
import { useNavigate } from "react-router-dom";
import { useCookies } from "react-cookie";
export default function Login() {
  const methods = useForm();
  const [cookies, setCookie] = useCookies(["token"]);
  let navigate = useNavigate();

  return (
    <div className="fondo">
      <CenterWrapper>
        <SmallCard>
          <SimpleForm
            content={loginForm}
            methods={methods}
            onSubmit={(data) => LoginUser(data, navigate, setCookie)}
          />
        </SmallCard>
      </CenterWrapper>
    </div>
  );
}
