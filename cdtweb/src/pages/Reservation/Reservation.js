import { useLocation } from "react-router-dom";
import useCalendarData from "../../api/hooks/useCalendarData";
import { useCookies } from "react-cookie";
import { Fragment } from "react";
import PageWrapper from "../../components/PageWrapper";
import { Navbar } from "../../components/Navbar";
import Taskbar from "../../components/Taskbar";
import Loading from "../../components/Loading";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import {generateTable} from "./functions";
import {dates} from "./functions"

export default function Reservation({}) {
  const search = useLocation().search;
  const rid = new URLSearchParams(search).get("rid");
  const sid = new URLSearchParams(search).get("sid");
  const date = new URLSearchParams(search).get("date");
  const [cookies, setCookie, removeCookie] = useCookies(["token"]);
  const data = useCalendarData(cookies["token"], sid, rid, date);
  if (!data.loading && data.layout) {
  let calendarDates = dates(new Date(date))
  let tabledata = generateTable(data, calendarDates);
    return (
      <Fragment>
        <Navbar data={data.loggedInfo[0]} removeCookie={removeCookie} />
        <Taskbar title={"Reservar - " + data.resource.name} />
        <PageWrapper>
          <DataTable
            value={tabledata}
            selectionMode="single"
            cellSelection
            showGridlines
            responsiveLayout="scroll"
            onSelectionChange={(e) => console.log(e.value)}
          >
            <Column
              field="time"
              header="Horas"
              className="p-cell"
              style={{pointerEvents:"none"}}></Column>
            <Column field="monday" header={"Lunes " + calendarDates[0].toLocaleDateString()}></Column>
            <Column field="tuesday" header={"Martes " + calendarDates[1].toLocaleDateString()}></Column>
            <Column field="wednesday" header={"Miércoles " +  calendarDates[2].toLocaleDateString()} ></Column>
            <Column field="thursday" header={"Jueves " + calendarDates[3].toLocaleDateString()}></Column>
            <Column field="friday" header={"Viernes " + calendarDates[4].toLocaleDateString()}></Column>
            <Column field="saturday" header={"Sábado " + calendarDates[5].toLocaleDateString()}></Column>
            <Column field="sunday" header={"Domingo " +  calendarDates[6].toLocaleDateString()}></Column>
          </DataTable>
        </PageWrapper>
      </Fragment>
    );
  } else {
    return <Loading />;
  }
}
