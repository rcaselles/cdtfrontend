import { Tooltip } from "primereact/tooltip";
import { Fragment } from "react";
import { ContextMenu } from 'primereact/contextmenu';


function generateTable(data, calendarDates) {
  let tabledata = [];
  for (var time in data?.layout) {
    let startTime = data.layout[time].start_time.slice(0, -3);
    let fullStartTime = data.layout[time].start_time;
    let endTime = data.layout[time].end_time.slice(0, -3);
    let fullEndTime = data.layout[time].end_time;
    if (data.layout[time].availability_code != 2) {
      tabledata.push({
        time: startTime + " - " + endTime,
        monday: checkAvailable(
          data.reservations,
          toLocaleCustom(calendarDates[0]),
          fullStartTime,
          fullEndTime
        ),
        tuesday: checkAvailable(
          data.reservations,
          toLocaleCustom(calendarDates[1]),
          fullStartTime,
          fullEndTime
        ),
        wednesday: checkAvailable(
          data.reservations,
          toLocaleCustom(calendarDates[2]),
          fullStartTime,
          fullEndTime
        ),
        thursday: checkAvailable(
          data.reservations,
          toLocaleCustom(calendarDates[3]),
          fullStartTime,
          fullEndTime
        ),
        friday: checkAvailable(
          data.reservations,
          toLocaleCustom(calendarDates[4]),
          fullStartTime,
          fullEndTime
        ),
        saturday: checkAvailable(
          data.reservations,
          toLocaleCustom(calendarDates[5]),
          fullStartTime,
          fullEndTime
        ),
        sunday: checkAvailable(
          data.reservations,
          toLocaleCustom(calendarDates[6]),
          fullStartTime,
          fullEndTime
        ),
      });
    } else {
      //WARN: Check which time corresponds to this
      tabledata.push({
        time: startTime + " - " + endTime,
        monday: <NotAvailableCell />,
        tuesday: <NotAvailableCell />,
        wednesday: <NotAvailableCell />,
        thursday: <NotAvailableCell />,
        friday: <NotAvailableCell />,
        saturday: <NotAvailableCell />,
        sunday: <NotAvailableCell />,
      });
    }
  }
  return tabledata;
}

function checkAvailable(reservations, date, startTime, endTime) {
  for (let i = 0; i < reservations.length; i++) {
    let element = reservations[i];
    if (
      element.start_date == date + ", " + startTime &&
      element.end_date == date + ", " + endTime
    ) {
      return (
        <BusyCell className={"cell" + element.series_id}>
          &nbsp;{" "}
          <Tooltip target={".cell" + element.series_id}>
            {generateTooltipValue(element)}
          </Tooltip>
        </BusyCell>
      );
    }
  }
}

//TODO: GDPR data protection
function generateTooltipValue(element) {
  return (
    <Fragment>
      <b>
        {element.fname} {element.lname}
      </b>
      <br />
      <b>{element.title}</b>
      <br />
      {element.start_date.substr(-8).slice(0, -3)} -{" "}
      {element.end_date.substr(-8).slice(0, -3)}
    </Fragment>
  );
}

/*
 *     INFO: If classname is empty don't show any classes
 */
function BusyCell({ children, className = "" }) {
  return (
    <div style={{ backgroundColor: "#D53E3E" }} className={className}>
      &nbsp;{children}
    </div>
  );
}

function NotAvailableCell({children}) {
  return (
    <div style={{ backgroundColor: "darkred" }}>
    &nbsp;{children}
  </div>
  )
}

function toLocaleCustom(date) {
  return date.toLocaleString("es", {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
  });
}
function dates(current) {
  let currentAux = new Date(getMonday(current));
  var week = [];
  for (var i = 0; i < 7; i++) {
    week.push(new Date(+currentAux));
    currentAux.setDate(currentAux.getDate() + 1);
  }
  return week;
}

function getMonday(d) {
  d = new Date(d);
  var day = d.getDay(),
    diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}
export { generateTable, dates };
