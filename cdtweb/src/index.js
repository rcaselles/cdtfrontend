import React from "react";
import { Suspense } from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./i18n";
import "./index.css";

import { BrowserRouter } from "react-router-dom";
import Loading from "./components/Loading";
import { CookiesProvider } from "react-cookie";
import { Auth } from "./Auth";
ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Suspense fallback={<Loading />}>
        <CookiesProvider>
          <Auth>
            <App />
          </Auth>
        </CookiesProvider>
      </Suspense>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);
