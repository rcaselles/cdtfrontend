import { Fragment } from "react";
import { useCookies } from "react-cookie";
import { useLocation, useNavigate } from "react-router-dom";
import Loading from "./components/Loading";
export function Auth({ children }) {
  const [cookies, setCookie, removeCookie] = useCookies(["cookie-name"]);
  let location = useLocation();
  let navigate = useNavigate();
  if (cookies.token && location.pathname.substring(1) == "") {
    window.location.href = "/dashboard";
    return <Loading />;
  }
  if (
    typeof cookies.token == "undefined" &&
    location.pathname.substring(1) != ""
  ) {
    window.location.href = "/";
    return <Loading />;
  } else {
    return <Fragment>{children}</Fragment>;
  }
}
