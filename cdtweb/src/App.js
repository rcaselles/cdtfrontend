import { Routes, Route, useLocation } from "react-router-dom";
import Login from "./pages/Login/Login";
import { Fragment } from "react";
import "./index.css";
import "primereact/resources/themes/mdc-light-indigo/theme.css"; //theme
import "primereact/resources/primereact.min.css"; //core css
import "primeicons/primeicons.css"; //icons
import "primeflex/primeflex.css";
import { APP_NAME } from "./api/config";
import Dashboard from "./pages/Dashboard/Dashboard";
import Profile from "./pages/Profile/Profile";
import Reservation from "./pages/Reservation/Reservation";
function App() {
  let location = useLocation();
  let pathFinal =
    location.pathname === "/"
      ? "Login"
      : location.pathname.charAt(1).toUpperCase() +
        location.pathname.substring(2);
  return (
    <Fragment>
      <title>
        {pathFinal} - {APP_NAME}
      </title>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/profile" element={<Profile />} />
        <Route path="/reservation" element={<Reservation />} />
      </Routes>
    </Fragment>
  );
}

export default App;
