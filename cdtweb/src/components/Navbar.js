import { Menubar } from "primereact/menubar";
import { t } from "i18next";
export function Navbar({ removeCookie, data }) {
  const items = [
    {
      label: t("Start"),
      icon: "pi pi-fw pi-home",
      command: () => (window.location.href = "/dashboard"),
    },
    {
      label: t("Book"),
      icon: "pi pi-fw pi-calendar-plus",
    },
    {
      label: t("My Calendar"),
      icon: "pi pi-fw pi-calendar",
    },
  ];
  //Admin part
  if (data?.isadmin == true) {
    items.push({
      label: t("Admin Panel"),
      icon: "pi pi-fw pi-lock",
    });
  }

  const start = (
    <img
      alt="logo"
      src="logo/logo.png"
      height="50"
      onClick={() => (window.location.href = "/dashboard")}
      className="mr-2 cursor-pointer"
    ></img>
  );
  const end = (
    <ul className="p-menubar-root-list" role="menubar">
      <li role="none" className="p-menuitem">
        <p className="p-menuitem">
          <span className="p-menuitem-text">
            <b>Sunpad:</b> {data?.sunpad}
          </span>
        </p>
      </li>
      <li role="none" className="p-menuitem">
        <a
          href="#"
          role="menuitem"
          className="p-menuitem-link"
          aria-haspopup="false"
          onClick={() => {
            window.location.href = "/profile";
          }}
        >
          <span className="p-menuitem-icon pi pi-fw pi-user"></span>
          <span className="p-menuitem-text">{t("My profile")}</span>
        </a>
      </li>
      <li role="none" className="p-menuitem">
        <a
          href="#"
          role="menuitem"
          className="p-menuitem-link"
          aria-haspopup="false"
          onClick={() => {
            removeCookie("token");
            window.location.reload();
          }}
        >
          <span className="p-menuitem-icon pi pi-fw pi-sign-out"></span>
          <span className="p-menuitem-text">{t("Close session")}</span>
        </a>
      </li>
    </ul>
  );

  return (
    <div>
      <div className="card">
        <Menubar model={items} start={start} end={end} />
      </div>
    </div>
  );
}
