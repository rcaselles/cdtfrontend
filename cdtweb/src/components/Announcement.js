import { Fragment } from "react";

export default function Announcement({ announcement }) {
  if (!announcement) return <Fragment />;
  let textSum = announcement.announcement_text;
  if (textSum.length > 250) {
    textSum = textSum.substring(0, 250) + "...";
  }
  return (
    <div className="p-message p-component p-message-info p-message-enter-done">
      <div className="p-message-wrapper">
        <span className="p-message-icon pi  pi-info-circle"></span>
        <span className="p-message-summary">
          <b>{announcement.announcement_title}&nbsp;</b>
        </span>
        <span className="p-message-detail">
          {textSum}
          {textSum.length <= 253 ? <b>[Leer más]</b> : null}
        </span>
      </div>
    </div>
  );
}
