export default function PageWrapper({ children }) {
  return <div className="p-2 items-center">{children}</div>;
}
