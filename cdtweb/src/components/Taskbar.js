import React from "react";
import { Toolbar } from "primereact/toolbar";
import { t } from "i18next";
import Loading from "./Loading";
import { ProfileActions } from "./TaskbarActions/Profile";

export default function Taskbar({ title, setPage }) {
  const leftContents = (
    <React.Fragment>
      <h3>{t(title)}</h3>
    </React.Fragment>
  );

  const rightContents = getRightContents(title, setPage);

  return (
    <Toolbar
      left={leftContents}
      right={rightContents}
      style={{ backgroundColor: "#f3f3f3", padding: "0.25rem" }}
    />
  );
}

function getRightContents(title, setPage) {
  switch (title) {
    case "Profile":
      return <ProfileActions hook={setPage} />;
  }
}
