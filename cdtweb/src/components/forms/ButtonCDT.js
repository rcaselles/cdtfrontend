import { Button } from "primereact/button";
import { useTranslation } from "react-i18next";

function getColor(color) {
  switch (color) {
    case "danger":
      return "p-button-danger";
    default:
      return "";
  }
}
export default function ButtonCDT({ label, icon, onClick = null, color = "" }) {
  const { t } = useTranslation();
  //En formularios por el react-hook-form siempre es submit
  return (
    <Button
      label={t(label)}
      icon={icon}
      type="submit"
      onClick={onClick}
      className={"w-full " + getColor(color)}
    />
  );
}
