import { FormProvider } from "react-hook-form";
import ButtonCDT from "./ButtonCDT";
import InputLabel from "./InputLabel";
import LinkCDT from "./LinkCDT";
import TitleCDT from "./TitleCDT";
export default function SimpleForm({ methods, onSubmit, content }) {
  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onSubmit)}>
        {content["data"].map((field, key) => {
          switch (field.type) {
            case "title":
              return (
                <TitleCDT
                  text={field.text}
                  extrastyle={field.extrastyles}
                  key={key}
                />
              );
            case "inputlabel":
              return (
                <InputLabel
                  type={field.inputtype}
                  name={field.name}
                  key={key}
                  text={field.label}
                />
              );
            case "link":
              return <LinkCDT href="#" text={field.text} key={key} />;
            case "button":
              return (
                <ButtonCDT
                  label={field.label}
                  icon={field.icon}
                  key={key}
                  color={field.color}
                />
              );
            default:
              return <></>;
          }
        })}
      </form>
    </FormProvider>
  );
}
