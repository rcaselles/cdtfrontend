import { InputText } from "primereact/inputtext";
import { useFormContext } from "react-hook-form";

export default function InputCDT({ type, name }) {
  const { register } = useFormContext();
  return (
    <InputText
      type={type}
      className="w-full mb-3"
      {...register(name)}
      required
    />
  );
}
