export default function SmallCard({ children }) {
  return (
    <div className="surface-card p-4 shadow-2 border-round mt-5 mb:w-15 lg:w-3">
      {children}
    </div>
  );
}
