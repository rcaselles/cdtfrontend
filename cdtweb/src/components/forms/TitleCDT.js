import { useTranslation } from "react-i18next";
export default function TitleCDT({ text, extrastyle }) {
  const { t } = useTranslation();

  return (
    <div className={"mb-5 text-900 text-4xl font-medium " + extrastyle}>
      {t(text)}
    </div>
  );
}
