import LabelCDT from "../../components/forms/LabelCDT";
import InputCDT from "../../components/forms/InputCDT";
import { Fragment } from "react";
import { useTranslation } from "react-i18next";

export default function InputLabel({ type, name, text }) {
  const { t } = useTranslation();
  return (
    <Fragment>
      <LabelCDT label={t(text)} />
      <InputCDT type={type} name={name} />
    </Fragment>
  );
}
