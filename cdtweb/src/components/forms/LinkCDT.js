import { useTranslation } from "react-i18next";

export default function LinkCDT({ href, text }) {
  const { t } = useTranslation();

  return (
    <a
      className="flex mb-6 font-medium no-underline text-blue-500 cursor-pointer"
      href={href}
    >
      {t(text)}
    </a>
  );
}
