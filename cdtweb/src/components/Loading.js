import CenterWrapper from "./CenterWrapper";
import { ProgressSpinner } from "primereact/progressspinner";
import { Fragment } from "react";
export default function Loading() {
  return (
    <div className="justify-center text-center h-50 items-center mt-50">
      <ProgressSpinner
        style={{ width: "50px", height: "50px", marginTop: "10%" }}
        strokeWidth="6"
        fill="var(--surface-ground)"
        className=""
        animationDuration=".5s"
      />
      <br />
      <p className="text-lg mt-4 font-bold text-slate-900">Cargando....</p>
    </div>
  );
}
