import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Tag } from "primereact/tag";
import { t } from "i18next";
import { useState } from "react";

export default function ResourceTable({ data }) {
  const [element, setElement] = useState(null);

  for (var resource of data) {
    if (resource.enabled == 1) {
      resource.tag = <Tag severity="success" value={t("Available")}></Tag>;
      resource.participants = resource.max_participants + " " + t("people");
      resource.duration = resource.max_duration / 60 + " " + t("minutes");
    } else {
      resource.tag = <Tag severity="danger" value={t("Unavailable")}></Tag>;
      resource.participants = "-";
      resource.duration = "-";
    }
  }
  return (
    <div>
      <div className="card">
        <DataTable
          value={data}
          responsiveLayout="scroll"
          selectionMode="single"
          selection={element}
          onSelectionChange={(e) => {
            setElement(e.value);
            console.log(e.value);
            window.location.href =
              "/reservation?rid=" +
              e.value.resource_id +
              "&sid=" +
              e.value.schedule_id +
              "&date=" +
              new Date().toISOString();
          }}
        >
          <Column field="name" header={t("Name")}></Column>
          <Column field="tag" header={t("Availability")}></Column>
          <Column field="participants" header={t("Participants")}></Column>
          <Column field="duration" header={t("Duration")}></Column>
        </DataTable>
      </div>
    </div>
  );
}
