import React, { Component } from "react";
import { Button } from "primereact/button";
import { t } from "i18next";

const isMobile = () => window.matchMedia && window.matchMedia("(max-width: 480px)").matches;
export const ProfileActions = ({hook}) => {
        return <React.Fragment>
          <Button
            icon="pi pi-pencil"
            className="mr-2"
            label={isMobile() ? "" : t("Edit profile")}
            onClick={() => hook("edit")}
          />
          <Button
            icon="pi pi-bell"
            className="p-button-success mr-2"
            label={isMobile() ? "" : t("Notification preferences")}
          />
          <Button
            icon="pi pi-lock "
            className="p-button-danger"
            label={isMobile() ? "" : t("Edit password")}
            onClick={() => hook("password")}
          />
        </React.Fragment>
} 