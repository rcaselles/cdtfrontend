export default function CenterWrapper({ children }) {
  return (
    <div className="flex align-items-center justify-content-center">
      {children}
    </div>
  );
}
