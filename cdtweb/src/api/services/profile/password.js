import Swal from "sweetalert2";
import { URL } from "../../../api/config";
import { confirmDialog } from "primereact/confirmdialog";

export default function updatePassword(data, cookie) {
  if (data.password !== data.password2) {
    return Swal.fire({
      title: "Error",
      text: "La nueva contraseña no coincide",
      icon: "error",
      confirmButtonText: "Vale",
    });
  } else {
    console.table(data);
  }
  fetch(URL + "users/password", {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "access-token": cookie,
    },
    method: "POST",
    body: JSON.stringify(data),
  })
    .then(function (res) {
      if (res.status == 200) {
        confirmDialog({
          message: "Guardado!",
          header: "Éxito",
          icon: "pi pi-check",
          rejectLabel: " ",
          acceptLabel: "Ok",
        });
        window.location.reload();
      } else {
        confirmDialog({
          message: "Hay algún dato incorrecto",
          header: "Éxito",
          icon: "pi pi-exclamation-triangle",
          rejectLabel: " ",
          acceptLabel: "Ok",
        });
      }
    })
    .catch(function (res) {
      confirmDialog({
        message: "Hay algún dato incorrecto",
        header: "Éxito",
        icon: "pi pi-exclamation-triangle",
        rejectLabel: " ",
        acceptLabel: "Ok",
      });
    });
}
