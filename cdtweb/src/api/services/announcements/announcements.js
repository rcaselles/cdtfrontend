import { URL } from "../../../api/config";
export default async function getAnnouncements(cookie, extras) {
  const res = await fetch(URL + "announcements/" + extras, {
    method: "GET",
    headers: { "access-token": cookie },
  });

  return res.json();
}
