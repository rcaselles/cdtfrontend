import { URL } from "../../config";
export default async function getLayouts(schedule_id, cookie) {
  const res = await fetch(URL + "layouts/" + schedule_id, {
    method: "GET",
    headers: { "access-token": cookie },
  });
  return await res.json();
}
