import { URL } from "../../config";
export default async function getReservations(resource_id, date, cookie) {
  const res = await fetch(
    URL +
      "reservations?rid=" +
      resource_id +
      "&date=" +
      new Date(date).toISOString(),
    {
      method: "GET",
      headers: { "access-token": cookie },
    }
  );
  return await res.json();
}
