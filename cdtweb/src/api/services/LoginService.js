import { URL } from "../config";
import { confirmDialog } from "primereact/confirmdialog";

export default async function LoginUser(data, navigate, setCookie) {
  await fetch(URL + "login?user=" + data.email + "&password=" + data.password, {
    method: "POST",
  })
    .then((resp) => resp.json())
    .then((data) => {
      if (data.status === "error") {
        confirmDialog({
          message: "Ha habido un error: " + data.error,
          header: "Error",
          icon: "pi pi-exclamation-triangle",
          rejectLabel: " ",
          acceptLabel: "Ok",
        });
      } else {
        setCookie("token", data.token, { path: "/", maxAge: 86400 });
        return navigate("/dashboard");
      }
    })
    .catch((error) => {
      confirmDialog({
        message: "Ha habido un error de conexión",
        header: "Error",
        icon: "pi pi-exclamation-triangle",
        rejectLabel: " ",
        acceptLabel: "Ok",
      });
    });
}
