import { URL } from "../../config";
export default async function getResources(resource_id = "", cookie) {
  const res = await fetch(URL + "resources/padel/" + resource_id, {
    method: "GET",
    headers: { "access-token": cookie },
  });
  return await res.json();
}
