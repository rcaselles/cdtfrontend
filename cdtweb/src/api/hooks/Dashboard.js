import getLogged from "../services/logged/profile";
import getAnnouncements from "../services/announcements/announcements";
import getResources from "../services/resources/resources";

export default async function getDashboardData(cookie) {
  const [loggedInfo, announcement, resources] = await Promise.all([
    getLogged(cookie),
    getAnnouncements(cookie),
    getResources("", cookie),
  ]);

  return {
    loggedInfo: loggedInfo[0],
    announcement: announcement[0],
    resources: resources,
  };
}
