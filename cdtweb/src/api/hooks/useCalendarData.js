import { useState, useEffect } from "react";

import getLogged from "../services/logged/profile";
import getLayouts from "../services/layouts/layouts";
import getReservations from "../services/reservations/reservations";
import getResources from "../services/resources/resources";

const useCalendarData = (token, sid, rid, date = new Date().toISOString()) => {
  const schedule_id = sid;
  const resource_id = rid;
  const [logged, setLogged] = useState([]);
  const [reservations, setReservations] = useState([]);
  const [layout, setLayout] = useState([]);
  const [resources, setResources] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (sid && rid && date) {
      Promise.allSettled([
        getLogged(token),
        getReservations(resource_id, date, token),
        getLayouts(schedule_id, token),
        getResources(resource_id, token),
      ]).then(([logged, reservations, layout, resource]) => {
        const setCallback = (item, callback) => {
          if (item.status === "fulfilled") {
            callback(item.value);
          } else {
            console.error(item.reason);
          }
        };
        setCallback(logged, setLogged);
        setCallback(reservations, setReservations);
        setCallback(layout, setLayout);
        setCallback(resource, setResources);
        setLoading(false);
      });
    }
  }, [resource_id, schedule_id, token, date, rid, sid]);
  return {
    loggedInfo: logged,
    reservations,
    layout,
    resource: resources,
    loading,
  };
};
export default useCalendarData;
