const TelegramBot = require("node-telegram-bot-api");
const token = "5384184249:AAFKZiQ-FYExh_fm1ZLmUeU62hqsf2EsGBc";
const bot = new TelegramBot(token, { polling: true });
const TorrentSearchApi = require("torrent-search-api");
// Habilitamos todo lo publico
TorrentSearchApi.enablePublicProviders();
bot.onText(/\/sitios/, (msg) => {
  let activeProviders = TorrentSearchApi.getActiveProviders();
  let message = "<b>Busco en estos sitios:</b>\n";
  for (provider of activeProviders) {
    message += provider.name + "\n";
  }
  bot.sendMessage(msg.chat.id, message, { parse_mode: "HTML" });
});
bot.onText(/^\/descargar (.+)/, function (msg, match) {
  var text = match[1];
  getTorrent(text).then((result) => {
    let message = "<b>Te he encontrado esto: </b>\n";
    let i = 1;
    for (torrent of result) {
      message += "<b>[" + torrent.provider + "]</b> " + i + " - ";
      torrent.title + "(Tamaño: " + torrent.size + ") \n";
      i++;
    }
    bot.sendMessage(msg.chat.id, message, {
      parse_mode: "HTML",
    });
  });
});
async function getTorrent(text = "") {
  const torrents = await TorrentSearchApi.search(text, "All", 10);
  return torrents;
}
bot.on("message", (msg) => {
  //bot.sendMessage(msg.chat.id, "Aun no sé nada");
});
