import React from "react";

const CDTContext = React.createContext({
  errores: [],
  setErrores: () => {},
});

export default CDTContext;
