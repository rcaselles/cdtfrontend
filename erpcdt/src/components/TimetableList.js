import { Fragment } from "react";

export default function Timelist({ calendarData }) {
  return (
    <div className="tablelist">
      <div className="flex flex-col p-2 ml-2" style={{ width: "98%" }}>
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="inline-block py-2 min-w-full sm:px-6 lg:px-8">
            <div className="overflow-hidden shadow-md sm:rounded-lg">
              <table className="min-w-full">
                <thead className="bg-gray-50 dark:bg-gray-700">
                  <tr>
                    <th
                      scope="col"
                      className="py-1 px-4 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                    >
                      Titulo
                    </th>
                    <th
                      scope="col"
                      className="py-1 px-4 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                    >
                      Inicio
                    </th>
                    <th
                      scope="col"
                      className="py-1 px-4 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                    >
                      Fin
                    </th>
                    <th
                      scope="col"
                      className="py-1 px-4 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
                    >
                      Usuario
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {calendarData.reservations.map((item, index) => (
                    <tr
                      className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
                      key={index}
                    >
                      <td className="py-1 px-4 text-sm font-medium text-gray-900 dark:text-white">
                        {item.title}
                      </td>
                      <td className="py-1 px-4 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                        {item.start_date}
                      </td>
                      <td className="py-1 px-4 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                        {item.end_date}
                      </td>
                      <td className="py-1 px-4 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                        {item.fname} {item.lname}
                      </td>
                    </tr>
                  ))}
                  {calendarData.reservations.length == 0 ? (
                    <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                      <div className="font p-1">No hay reservas</div>
                    </tr>
                  ) : (
                    <Fragment />
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>{" "}
    </div>
  );
}
