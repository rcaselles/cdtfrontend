import { useContext } from "react";
import CDTContext from "../../contexts/CDTContext";
const ErrorsComponent = ({ children }) => {
  const { errores } = useContext(CDTContext);
  return (
    <>
      {errores.map((item, index) => (
        <Error key={index} mensajeError="DA UN ERROR QUE FLIPAS LOL" />
      ))}
      {children}
    </>
  );
};

const Error = ({ mensajeError }) => {
  return (
    <div style={{ backgroudColor: "red", width: "100%", minHeight: "20px" }}>
      mensajeError
    </div>
  );
};
export default ErrorsComponent;
