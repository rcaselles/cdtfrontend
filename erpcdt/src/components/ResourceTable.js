import { goTo } from "../utils/functions";
import { useRouter } from "next/router";
import TableHeader from "./ResourceTable/Header";
export default function ResourceTable(props) {
  const router = useRouter();
  return (
    <table className="min-w-full divide-y divide-gray-200">
      <TableHeader></TableHeader>
      <tbody className="bg-white divide-y divide-gray-200">
        {Array.from(props.dashboardData?.resource).map((pista, index) => {
          return (
            <tr
              key={index}
              className="hover:bg-blue-100 cursor-pointer"
              onClick={() => goTo(router, pista.resource_id, pista.schedule_id)}
            >
              <td className="px-5 py-2 whitespace-nowrap">
                <div className="text-sm font-medium text-gray-900">
                  {pista.name}
                </div>
              </td>
              <td className="px-5 py-2 whitespace-nowrap">
                <div className="text-sm text-gray-900">
                  {pista.max_duration / 60} minutos
                </div>
              </td>
              <td className="px-5 py-2 whitespace-nowrap">
                {pista.enabled ? (
                  <span className="px-5 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                    Activa
                  </span>
                ) : (
                  <span className="px-5 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-white-800">
                    Bloqueada
                  </span>
                )}
              </td>
              <td className="px-5 py-2 whitespace-nowrap text-sm text-gray-500">
                {pista.max_participants} Personas
              </td>
              <td className="px-5 py-2 whitespace-nowrap text-right text-sm font-medium">
                <a
                  href="#"
                  className="text-indigo-600 hover:text-white text-lg"
                >
                  &#10132;
                </a>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
