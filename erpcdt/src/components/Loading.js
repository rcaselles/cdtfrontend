import { Spinner } from "react-bootstrap";

export default function Loading() {
  return (
    <div className="justify-center text-center h-50 items-center my-50px">
      <Spinner animation="border" variant="primary" />
      <br />
      <br />
      <p className="text-lg font-medium text-slate-900">Cargando....</p>
    </div>
  );
}
