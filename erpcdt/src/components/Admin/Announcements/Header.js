export default function Header() {
  return (
    <thead className="bg-gray-50 dark:bg-gray-700">
      <tr>
        <th
          scope="col"
          className="py-2 px-5 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
        >
          Texto del anuncio
        </th>
        <th
          scope="col"
          className="py-2 px-5 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
        >
          Titulo
        </th>
        <th
          scope="col"
          className="py-2 px-5 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
        >
          Fecha de inicio
        </th>
        <th
          scope="col"
          className="py-2 px-5 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
        >
          Fecha de fin
        </th>
        <th scope="col" className="relative py-2 px-6">
          <span className="sr-only">Edit</span>
        </th>
      </tr>
    </thead>
  );
}
