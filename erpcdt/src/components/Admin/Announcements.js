import useAnnouncementsData from "../../api/hooks/useAnnouncementsData";
import Loading from "../Loading";
import Header from "./Announcements/Header";
import Table from "./Announcements/Table";
export default function Announcements({}) {
  const { announcements, loading } = useAnnouncementsData();
  const truncate = (str, max) => {
    return str.length > max ? str.substring(0, max) + "..." : str;
  };

  if (loading == false) {
    return (
      <div className="p-3">
        <div className="flex flex-row-reverse">
          <button
            type="button"
            className="text-white bg-gray-800 hover:bg-gray-900 focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-3 py-2.5 text-center mr-2 mb-2 dark:bg-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-800 dark:border-gray-700"
          >
            Añadir anuncio
          </button>
        </div>
        <Table>
          <Header />
          <tbody>
            {announcements.map((announcement, key) => {
              return (
                <tr
                  className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
                  key={key}
                >
                  <td className="py-2 px-5 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400 truncate ">
                    {truncate(announcement.announcement_text, 20)}
                  </td>
                  <td className="py-2 px-5 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                    {truncate(announcement.announcement_title, 20)}
                  </td>
                  <td className="py-2 px-5 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                    {new Date(announcement.start_date).toLocaleString()}
                  </td>
                  <td className="py-2 px-5 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                    {new Date(announcement.end_date).toLocaleString()}
                  </td>
                  <td className="py-2 px-5 text-sm font-medium text-right whitespace-nowrap">
                    <a
                      href="#"
                      className="text-blue-600 dark:text-blue-500 hover:underline"
                    >
                      Editar
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a
                      href="#"
                      className="text-blue-600 dark:text-blue-500 hover:underline"
                    >
                      Eliminar
                    </a>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  } else {
    return <Loading />;
  }
}
