import { version } from "react";
import packageJson from "../../../package.json";
export default function Inicio({}) {
  let dependencies = "";
  Object.entries(packageJson.dependencies).forEach((item, key) => {
    if (key != Object.keys(packageJson.dependencies).length - 1) {
      dependencies += item[0] + " (" + item[1] + "), ";
    } else {
      dependencies += item[0] + " (" + item[1] + ").";
    }
  });
  return (
    <div className="p-3">
      <div>
        <b>Horario actual del servidor: </b>
        {new Date().toLocaleString()}
      </div>
      <div>
        <b>Versión del framework React: </b>
        {version}
      </div>
      <div>
        <b>Dependencias activas: </b>
        {dependencies}
      </div>
    </div>
  );
}
