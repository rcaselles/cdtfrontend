import updateProfile from "../api/services/profile/profile";

export function ProfileCard({ register }) {
  return (
    <div className="p-4 w-fit bg-white rounded-lg border shadow-md sm:p-6  m-3 dark:bg-gray-800 dark:border-gray-700">
      <form onSubmit={handleSubmit(updateProfile)} id="perfil">
        <div className="relative z-0 mb-6 w-full group">
          <label
            htmlFor="floating_email"
            className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
          >
            Correo electrónico
          </label>
          <input
            type="email"
            name="floating_email"
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            defaultValue={profile[0].email}
            {...register("email")}
            required
          />
        </div>
        <div className="relative z-0 mb-6 w-full group">
          <label
            htmlFor="floating_username"
            className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
          >
            Usuario
          </label>
          <input
            type="text"
            name="floating_username"
            id="floating_username"
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
            defaultValue={profile[0].username}
            {...register("username")}
            required
          />
        </div>
        <div className="grid xl:grid-cols-2 xl:gap-6">
          <div className="relative z-0 mb-6 w-full group">
            <label
              htmlFor="floating_first_name"
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
            >
              Nombre
            </label>
            <input
              type="text"
              name="floating_first_name"
              id="floating_first_name"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              defaultValue={profile[0].fname}
              {...register("fname")}
              required
            />
          </div>
          <div className="relative z-0 mb-6 w-full group">
            <label
              htmlFor="floating_last_name"
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
            >
              Apellidos
            </label>
            <input
              type="text"
              name="floating_last_name"
              id="floating_last_name"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              defaultValue={profile[0].lname}
              {...register("lname")}
              required
            />
          </div>
        </div>
        <div className="grid xl:grid-cols-2 xl:gap-6">
          <div className="relative z-0 mb-6 w-full group">
            <label
              htmlFor="floating_phone"
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
            >
              Número de teléfono
            </label>
            <input
              type="tel"
              name="floating_phone"
              id="floating_phone"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              defaultValue={profile[0].phone}
              {...register("phone")}
              required
            />
          </div>
          <div className="relative z-0 mb-6 w-full group">
            <label
              htmlFor="floating_company"
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
            >
              Compañía
            </label>
            <input
              type="text"
              name="floating_company"
              id="floating_company"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              defaultValue={profile[0].organization}
              {...register("company")}
              required
            />
          </div>
        </div>
        <button
          type="submit"
          className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
          Guardar
        </button>
      </form>
    </div>
  );
}
