import { findOccupied, dateList } from "../utils/functions";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import TableHeader from "./Timetable/Header";
import Cell from "./Timetable/Cell";

export default function Timetable({ date, calendarData }) {
  return (
    <div className="timetable" style={{ width: "98.50%" }}>
      <div className="flex flex-col text-center">
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-7">
          <div className="inline-block min-w-screen sm:p-10 lg:p-10">
            <div className="overflow-hidden shadow-md sm:rounded-lg">
              <table className="min-w-screen p-1">
                <TableHeader date={date}></TableHeader>
                <tbody>
                  {Array.from(calendarData.layout).map((item, index) => {
                    // Item is the whole week in one slot, for example: from 9:00 - 11:00 from Monday to Sunday
                    const fechas = dateList(date);
                    let fechasAux = new Array();
                    for (const date in fechas) {
                      fechasAux.push(
                        fechas[date].toLocaleDateString("es-ES", {
                          year: "numeric",
                          month: "2-digit",
                          day: "2-digit",
                        }) +
                          " " +
                          item.start_time
                      );
                    }
                    let styles = new Array();
                    let datos = new Array();

                    for (const element in fechasAux) {
                      //Comparing week reservations with layout
                      const query = findOccupied(
                        fechasAux[element],
                        calendarData.reservations
                      );

                      if (query.status == true) {
                        //Not available
                        if (item.availability_code == 2) {
                          styles.push(" bg-red-400 select-none");
                        } else {
                          styles.push(" bg-blue-500 cursor-pointer");
                        }
                        datos.push(query.element);
                      } else {
                        if (item.availability_code == 2) {
                          styles.push(" bg-red-400 select-none");
                        } else {
                          styles.push(" hover:bg-blue-500 cursor-pointer");
                        }
                        datos.push("");
                      }
                    }
                    return (
                      <tr
                        className="bg-white border-solid dark:bg-gray-800 dark:border-gray-700"
                        key={index + "tabla"}
                      >
                        <td className="border-solid border-2 border-light-gray-500 py-1 px-2 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400 select-none">
                          {item.start_time} - {item.end_time}
                        </td>
                        {datos.map((dato, index) => {
                          return (
                            <Cell
                              datos={dato}
                              key={index}
                              item={item}
                              index={index}
                              styles={styles}
                            ></Cell>
                          );
                        })}
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
