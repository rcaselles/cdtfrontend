import { dateList } from "../../utils/functions";

export default function TableHeader(props) {
  return (
    <thead className="bg-gray-50 dark:bg-gray-700">
      <tr>
        <th
          scope="col"
          className="select-none border-solid border-2 border-light-gray-500 py-1 px-2 text-xs font-medium text-left text-gray-700 uppercase dark:text-gray-400"
        >
          Horario
        </th>
        <th
          scope="col"
          className="select-none w-32 border-solid border-2 border-light-gray-500 py-1 px-2 text-xs font-medium tracking-wider text-center text-gray-700 uppercase dark:text-gray-400"
        >
          Lunes<br></br>
          {dateList(props.date)[0].toLocaleDateString()}
        </th>
        <th
          scope="col"
          className="select-none w-32 border-solid border-2 border-light-gray-500 py-1 px-2  text-xs font-medium tracking-wider text-center text-gray-700 uppercase dark:text-gray-400"
        >
          Martes<br></br>
          {dateList(props.date)[1].toLocaleDateString()}
        </th>
        <th
          scope="col"
          className="select-none w-32 border-solid border-2 border-light-gray-500 py-1 px-2 text-xs font-medium tracking-wider text-center text-gray-700 uppercase dark:text-gray-400"
        >
          Miércoles<br></br>
          {dateList(props.date)[2].toLocaleDateString()}
        </th>
        <th
          scope="col"
          className="select-none w-32 border-solid border-2 border-light-gray-500 py-1 px-2  text-xs font-medium tracking-wider text-center text-gray-700 uppercase dark:text-gray-400"
        >
          Jueves<br></br>
          {dateList(props.date)[3].toLocaleDateString()}
        </th>
        <th
          scope="col"
          className="select-none w-32 border-solid border-2 border-light-gray-500 py-1 px-2 text-xs font-medium tracking-wider text-center text-gray-700 uppercase dark:text-gray-400"
        >
          Viernes<br></br>
          {dateList(props.date)[4].toLocaleDateString()}
        </th>

        <th
          scope="col"
          className="select-none w-32 border-solid border-2 border-light-gray-500 py-1 px-2 text-xs font-medium tracking-wider text-center text-gray-700 uppercase dark:text-gray-400"
        >
          Sábado<br></br>
          {dateList(props.date)[5].toLocaleDateString()}
        </th>
        <th
          scope="col"
          className="select-none w-32 border-solid border-2 border-light-gray-500 py-1 px-2 text-xs font-medium tracking-wider text-center text-gray-700 uppercase dark:text-gray-400"
        >
          Domingo<br></br>
          {dateList(props.date)[6].toLocaleDateString()}
        </th>
      </tr>
    </thead>
  );
}
