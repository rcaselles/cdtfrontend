import { Overlay, OverlayTrigger, Tooltip, ListGroup } from "react-bootstrap";
import { useRef, useState, useEffect } from "react";
export default function Cell(props) {
  const [show, setShow] = useState(false);
  const target = useRef(null);
  const email = "mailto:" + props.datos.email;
  function useOutsideAlerter(ref) {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setShow(false);
        }
      }
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }
  useOutsideAlerter(target);

  return (
    <>
      <OverlayTrigger
        key="bottom"
        placement="bottom"
        overlay={
          props.datos != "" ? (
            <Tooltip id="hoverall">
              <b>{props.datos.title}</b>
              <br></br>
              {props.datos.fname} {props.datos.lname}
              <br></br>
              {props.datos.email}
            </Tooltip>
          ) : (
            <Tooltip id="hoverall">
              {props.item.availability_code == 2
                ? "Horario no disponible"
                : "Este horario está disponible."}
            </Tooltip>
          )
        }
      >
        <td
          className={
            "border-solid border-2 border-light-gray-500 py-1 px-2 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400 " +
            props.styles[props.index]
          }
          ref={target}
          onClick={() => setShow(false)}
          onContextMenu={(event) => {
            event.preventDefault();
            if (props.datos != "") {
              setShow(!show);
              setTimeout(() => {
                setShow(false);
              }, 10000);
            }
          }}
        ></td>
      </OverlayTrigger>
      <Overlay target={target.current} show={show} placement="right">
        {({ placement, arrowProps, show: _show, popper, ...props }) => (
          <div
            {...props}
            style={{
              color: "white",
              borderRadius: 3,
              ...props.style,
            }}
          >
            <ListGroup>
              <ListGroup.Item
                action
                onClick={() => {
                  window.open(email);
                  setShow(!show);
                }}
              >
                ✉️ Enviar correo
              </ListGroup.Item>
              <ListGroup.Item
                action
                onClick={() => {
                  window.open(email);
                  setShow(!show);
                }}
              >
                ❌ Eliminar reserva
              </ListGroup.Item>
            </ListGroup>
            ,
          </div>
        )}
      </Overlay>
    </>
  );
}
