import React, { Fragment } from "react";
import { dateList } from "../utils/functions";
import { Disclosure } from "@headlessui/react";
import { useRouter } from "next/router";

export default function Taskbar({ title, hook, hookVal }) {
  const router = useRouter();
  const current = router.pathname.substring(1);
  let date = new Date(router.query.date).toString();
  let arrDate = dateList(date);
  function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
  }
  const navigation = [];

  function goBack() {
    let date = new Date(router.query.date);
    date.setDate(date.getDate() - 7);
    router.query.date = date.toISOString();
    router.push(router);
  }

  function goForward() {
    let date = new Date(router.query.date);
    date.setDate(date.getDate() + 7);
    router.query.date = date.toISOString();
    router.push(router);
  }

  switch (current) {
    case "Calendar":
      navigation.push(
        {
          name: "Cambio de vista",
          onClick: () => hook(!hookVal),
          styles: "cursor-pointer",
          current: false,
        },
        {
          name: "Imprimir",
          href: "#",
          styles: "",
          current: false,
        },
        {
          name: "<",
          href: "#",
          onClick: () => goBack(),
          styles: "",
          current: false,
        },
        {
          name:
            arrDate[0].toLocaleDateString() +
            " - " +
            arrDate[6].toLocaleDateString(),
          styles: "cursor-normal hover:no-underline",
          current: true,
        },
        {
          name: ">",
          href: "#",
          onClick: () => goForward(),
          styles: "",
          current: false,
        }
      );
      break;
    case "Admin":
      navigation.push(
        {
          name: "Inicio",
          onClick: () => hook("Inicio"),
          styles: "cursor-pointer",
          current: hookVal == "Inicio",
        },
        {
          name: "Usuarios",
          onClick: () => hook("Usuarios"),
          styles: "cursor-pointer",
          current: hookVal == "Usuarios",
        },
        {
          name: "Recursos",
          onClick: () => hook("Recursos"),
          styles: "cursor-pointer",
          current: hookVal == "Recursos",
        },
        {
          name: "Anuncios",
          onClick: () => hook("Anuncios"),
          styles: "cursor-pointer",
          current: hookVal == "Anuncios",
        }
      );
      break;
  }

  if (current != "") {
    return (
      <Disclosure as="nav" className="bg-gray-200">
        {({ open }) => (
          <>
            <div className="max-w-screen">
              <div className="flex flex-row flex-shrink-0 items-center justify-between h-16 pl-3">
                <h5>
                  <b>
                    {title} - {hookVal}
                  </b>
                </h5>
                <div className="hidden md:block">
                  <div className=" flex items-end ml-20">
                    {navigation.map((item) => (
                      <a
                        key={item.name}
                        href={item.href}
                        onClick={item.onClick}
                        className={classNames(
                          item.current
                            ? "bg-gray-900 text-white"
                            : "text-gray-900 hover:bg-gray-700 hover:text-white",
                          "px-3 py-2 rounded-md text-sm font-medium select-none",
                          item.styles
                        )}
                        aria-current={item.current ? "page" : undefined}
                      >
                        {item.name}
                      </a>
                    ))}{" "}
                  </div>
                </div>
                <div className=" flex md:hidden">
                  {/* Mobile menu button */}
                  <Disclosure.Button className="bg-gray-800 inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                    <span className="sr-only">Open main menu</span>
                    {open ? (
                      <svg
                        className="w-6 h-6"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fillRule="evenodd"
                          d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                          clipRule="evenodd"
                        ></path>
                      </svg>
                    ) : (
                      <svg
                        className="w-6 h-6"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          fillRule="evenodd"
                          d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                          clipRule="evenodd"
                        ></path>
                      </svg>
                    )}
                  </Disclosure.Button>
                </div>
              </div>
            </div>

            <Disclosure.Panel className="md:hidden">
              <div className="px-2 pt-2 pb-3 space-y-1 sm:px-3">
                {navigation.map((item) => (
                  <Disclosure.Button
                    key={item.name}
                    as="a"
                    href={item.href}
                    className={classNames(
                      item.current
                        ? "bg-gray-900 text-white"
                        : "text-gray-300 hover:bg-gray-700 hover:text-white",
                      "block px-3 py-2 rounded-md text-base font-medium"
                    )}
                    aria-current={item.current ? "page" : undefined}
                  >
                    {item.name}
                  </Disclosure.Button>
                ))}
              </div>
            </Disclosure.Panel>
          </>
        )}
      </Disclosure>
    );
  }
  return <Fragment />;
}
