export default function CDTCard({ margin, children, marginTop }) {
  const styles =
    "p-4 w-fit bg-white rounded-lg border shadow-md sm:p-6 mt-" +
    marginTop +
    " mx-" +
    margin +
    " dark:bg-gray-800 dark:border-gray-700";
  return <div className={styles}>{children}</div>;
}
