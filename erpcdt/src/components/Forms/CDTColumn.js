export default function CDTColumn(props) {
  const className = "grid xl:grid-cols-" + props.number + " xl:gap-6";
  return <div className={className}>{props.children}</div>;
}
