import { FormProvider } from "react-hook-form";
import CDTColumn from "./CDTColumn";
import CDTInput from "./CDTInput";
import CDTButton from "./CDTButton";
/**? 
 *       let profileform = [
        {
          name: "Profile",
          form: [
            {
              columns: [
                {
                  fields: [
                    {
                      type: "email",
                      label: "Correo electrónico",
                      default: email,
                      name: "email",
                    },
                    {
                      type: "text",
                      label: "Usuario",
                      default: username,
                      name: "username",
                    },
                  ],
                },
                {
                  fields: [
                    {
                      type: "text",
                      label: "Nombre",
                      default: fname,
                      name: "fname",
                    },
                    {
                      type: "text",
                      label: "Apellidos",
                      default: lname,
                      name: "lname",
                    },
                  ],
                },
                {
                  fields: [
                    {
                      type: "tel",
                      label: "Número de teléfono",
                      default: phone,
                      name: "phone",
                    },
                    {
                      type: "text",
                      label: "Compañía",
                      default: organization,
                      name: "company",
                    },
                  ],
                },
              ],
            },
          ],
        },
      ];
*/
export default function CDTForm({ methods, submit, content, submitlabel }) {
  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(submit)}>
        {content.map((item, key) => {
          return (
            <CDTColumn number={item.fields.length} key={key}>
              {item.fields.map((field, key) => {
                if (field.default != "") {
                  return (
                    <CDTInput
                      type={field.type}
                      key={key}
                      label={field.label}
                      defaultValue={field.default}
                      name={field.name}
                    />
                  );
                } else {
                  return (
                    <CDTInput
                      type={field.type}
                      key={key}
                      label={field.label}
                      name={field.name}
                    />
                  );
                }
              })}
            </CDTColumn>
          );
        })}
        <CDTButton
          type="submit"
          disabled={!methods.formState.isDirty}
          text={submitlabel}
        />
      </form>
    </FormProvider>
  );
}
