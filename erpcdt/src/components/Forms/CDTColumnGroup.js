export default function CDTColumnGroup({ children }) {
  return <div className="relative z-0 mb-6 w-full group">{children}</div>;
}
