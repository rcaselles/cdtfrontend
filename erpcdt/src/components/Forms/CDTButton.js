export default function CDTButton({ type, disabled, text, onClick }) {
  return (
    <button
      type={type}
      disabled={disabled}
      onClick={onClick}
      className={
        "text-white bg-gray-700 hover:bg-gray-500 focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center disabled:cursor-normal"
      }
    >
      {text}
    </button>
  );
}
