export default function TableHeader(props) {
  return (
    <thead className="bg-gray-50">
      <tr>
        <th
          scope="col"
          className="py-2 px-5 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
        >
          Nombre
        </th>
        <th
          scope="col"
          className="py-2 px-5 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
        >
          Duración
        </th>
        <th
          scope="col"
          className="py-2 px-5 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
        >
          Estado
        </th>
        <th
          scope="col"
          className="py-2 px-5 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400"
        >
          Participantes
        </th>
        <th scope="col" className="relative px-5 py-2">
          <span className="sr-only">Acciones</span>
        </th>
      </tr>
    </thead>
  );
}
