import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { Fragment } from "react";
const swal = withReactContent(Swal);

export default function Announcement({ title, text }) {
  if (!title || !text) {
    return <Fragment />;
  } else {
    let textSum = text;
    if (textSum.length > 500) {
      textSum = textSum.substring(0, 250) + "....[Leer más]";
    }
    function readMore() {
      swal.fire({
        title: <p>{title}</p>,
        html: <p>{text}</p>,
        icon: "info",
      });
    }
    return (
      <div
        className="p-4 mb-4 text-sm text-blue-700 bg-blue-100 rounded-lg dark:bg-blue-200 dark:text-blue-800"
        role="alert"
        onClick={readMore}
      >
        <span className="font-medium">{title}</span>
        <br /> {textSum}
      </div>
    );
  }
}
