//import '../styles/globals.css'
import { useState } from "react";
import "tailwindcss/tailwind.css";
import "./dashboard.css";
import "bootstrap/dist/css/bootstrap.min.css";
import CDTContext from "../contexts/CDTContext";
import Errors from "../components/Errors/Errors";
import Navbar from "../components/Navbar";
import Head from "next/head";
import { APP_NAME } from "../api/config";
import { useRouter } from "next/router";

function MyApp({ Component, pageProps }) {
  const [errores, setErrores] = useState([]);
  const router = useRouter();
  const current = router.pathname.substring(1);

  return (
    <CDTContext.Provider
      value={{
        setErrores,
        errores,
      }}
    >
      <Navbar></Navbar>
      <Head>
        <title>
          {current} - {APP_NAME}
        </title>
      </Head>
      <Errors>
        <Component {...pageProps} />
      </Errors>
    </CDTContext.Provider>
  );
}

export default MyApp;
