import { useRouter } from "next/router";
import React, { Fragment, useState } from "react";
import useProfileData from "../api/hooks/useProfileData";
import Loading from "../components/Loading";
import Taskbar from "../components/Taskbar";
import { useForm } from "react-hook-form";
import updateProfile from "../api/services/profile/profile";
import CDTButton from "../components/Forms/CDTButton";
import CDTInput from "../components/Forms/CDTInput";
import CDTColumn from "../components/Forms/CDTColumn";
import CDTForm from "../components/Forms/CDTForm";
import CDTCard from "../components/Forms/CDTCard";

export default function Profile({}) {
  const { loggedInfo: profile, loading } = useProfileData();
  const methods = useForm();
  if (!loading) {
    return (
      <Fragment>
        <Taskbar title="Mi perfil" />
        <CDTCard margin="25%">
          <CDTForm
            methods={methods}
            submit={updateProfile}
            content={profile}
            submitlabel="Guardar"
          />
        </CDTCard>
      </Fragment>
    );
  } else {
    return <Loading />;
  }
}
