import { useRouter } from "next/router";
import React, { Fragment } from "react";
import Loading from "../components/Loading";
import Taskbar from "../components/Taskbar";
import { useForm } from "react-hook-form";
import updatePassword from "../api/services/profile/password";
import passwordform from "../utils/staticForms/password";
import CDTForm from "../components/Forms/CDTForm";
import CDTCard from "../components/Forms/CDTCard";

export default function Profile({}) {
  const methods = useForm();
  return (
    <Fragment>
      <Taskbar title="Cambiar contraseña" />
      <CDTCard margin="25%">
        <CDTForm
          methods={methods}
          submit={updatePassword}
          content={passwordform[0].form[0].columns}
          submitlabel="Guardar"
        />
      </CDTCard>
    </Fragment>
  );
}
