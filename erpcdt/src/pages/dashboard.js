import React from "react";
import Announcement from "../components/Announcement";
import ResourceTable from "../components/ResourceTable";
import useDashboardData from "../api/hooks/useDashboardData";
import Loading from "../components/Loading";

export default function Dashboard() {
  const { announcement, loading, loggedInfo, resource } = useDashboardData();

  if (!loading) {
    return (
      <>
        <div className="min-h-full">
          <main>
            <div className="max-w-8xl mx-auto py-1 sm:px-6 lg:px-8">
              <div className="px-4 py-6 sm:px-0">
                <Announcement
                  title={announcement?.announcement_title}
                  text={announcement?.announcement_text}
                />
              </div>
              <div className="flex flex-col">
                <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                  <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                      <ResourceTable
                        dashboardData={{ announcement, loggedInfo, resource }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </main>
        </div>
      </>
    );
  } else {
    return (
      <div style={{ marginTop: "40vh" }}>
        <Loading />
      </div>
    );
  }
}
