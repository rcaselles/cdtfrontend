import { useRouter } from "next/router";
import React, { Fragment, useState } from "react";
import Timetable from "../components/Timetable";
import Timelist from "../components/TimetableList";
import useCalendarData from "../api/hooks/useCalendarData";
import Loading from "../components/Loading";
import Taskbar from "../components/Taskbar";

export default function Calendar({}) {
  const {
    layout,
    loggedInfo: profile,
    reservations,
    resource: padel,
    loading,
  } = useCalendarData();
  const router = useRouter();
  const [view, setView] = useState(true);
  const date = router.query.date ? router.query.date : new Date().toISOString();
  if (!loading) {
    return (
      <Fragment>
        <Taskbar
          title={padel ? padel.name : "Pista"}
          hook={setView}
          hookVal={view}
        />
        {view ? (
          <Timetable
            calendarData={{ profile, padel, layout, reservations }}
            date={date}
          />
        ) : (
          <Timelist
            calendarData={{ profile, padel, layout, reservations }}
            date={date}
          />
        )}
      </Fragment>
    );
  } else {
    return <Loading />;
  }
}
