import Head from "next/head";
import Image from "next/image";
import Swal from "sweetalert2";
import { useRouter } from "next/router";
import { URL } from "../api/config";
import { getCookies, setCookies, removeCookies } from "cookies-next";
import { APP_NAME } from "../api/config";
export default function Home({ data }) {
  const router = useRouter();
  let current = "Login";
  console.log(URL);
  const login = async (event) => {
    event.preventDefault();
    const username = event.target.username.value;
    const password = event.target.password.value;
    await fetch(URL + "login?user=" + username + "&password=" + password, {
      method: "POST",
    })
      .then((resp) => resp.json())
      .then((data) => {
        if (data.status == "error") {
          Swal.fire({
            title: "Error",
            text: "Credenciales incorrectas",
            icon: "error",
            confirmButtonText: "Vale",
          });
        } else {
          setCookies("token", data.token);
          router.push("Dashboard");
        }
      })
      .catch((error) => {
        console.log(error);
        Swal.fire({
          title: "Error",
          text: "Credenciales incorrectas" + error,
          icon: "error",
          confirmButtonText: "Vale",
        });
      });
  };
  if (getCookies("token").token) {
    router.push("/Dashboard");
    return <h1>Redirigiendo...</h1>;
  } else
    return (
      <div
        className="flex flex-col mx-auto my-auto w-full h-full items-center justify-center align-middle"
        style={{
          marginTop: "0",
          height: "100vh",
          backgroundImage:
            "url('https://reservas.clubdetenisdenia.com/betaphp2/assets/img/curved-images/curved6.jpg')",
          overflow: "hidden",
          width: "100%",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
        }}
      >
        <Head>
          <title>
            {current} - {APP_NAME}
          </title>
        </Head>
        <div className="flex justify-center flex-col w-full max-w-md px-4 py-8 bg-white rounded-lg shadow-lg dark:bg-gray-800 sm:px-6 md:px-8 lg:px-10">
          <div className="self-center mb-6 text-xl font-bold text-gray-600 sm:text-4xl dark:text-white">
            <h5>Inicio de sesión</h5>
          </div>
          <div className="mt-8">
            <form onSubmit={login}>
              <div className="flex flex-col mb-2">
                <div className="flex relative ">
                  <span className="rounded-l-md inline-flex  items-center px-3 border-t bg-white border-l border-b  border-gray-300 text-gray-500 shadow-sm text-sm">
                    <svg
                      width="15"
                      height="15"
                      fill="currentColor"
                      viewBox="0 0 1792 1792"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M1792 710v794q0 66-47 113t-113 47h-1472q-66 0-113-47t-47-113v-794q44 49 101 87 362 246 497 345 57 42 92.5 65.5t94.5 48 110 24.5h2q51 0 110-24.5t94.5-48 92.5-65.5q170-123 498-345 57-39 100-87zm0-294q0 79-49 151t-122 123q-376 261-468 325-10 7-42.5 30.5t-54 38-52 32.5-57.5 27-50 9h-2q-23 0-50-9t-57.5-27-52-32.5-54-38-42.5-30.5q-91-64-262-182.5t-205-142.5q-62-42-117-115.5t-55-136.5q0-78 41.5-130t118.5-52h1472q65 0 112.5 47t47.5 113z"></path>
                    </svg>
                  </span>
                  <input
                    type="text"
                    id="username"
                    className=" rounded-r-lg flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                    placeholder="Tu correo electrónico o usuario"
                  />
                </div>
              </div>
              <div className="flex flex-col mb-6">
                <div className="flex relative ">
                  <span className="rounded-l-md inline-flex  items-center px-3 border-t bg-white border-l border-b  border-gray-300 text-gray-500 shadow-sm text-sm">
                    <svg
                      width="15"
                      height="15"
                      fill="currentColor"
                      viewBox="0 0 1792 1792"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M1376 768q40 0 68 28t28 68v576q0 40-28 68t-68 28h-960q-40 0-68-28t-28-68v-576q0-40 28-68t68-28h32v-320q0-185 131.5-316.5t316.5-131.5 316.5 131.5 131.5 316.5q0 26-19 45t-45 19h-64q-26 0-45-19t-19-45q0-106-75-181t-181-75-181 75-75 181v320h736z"></path>
                    </svg>
                  </span>
                  <input
                    type="password"
                    id="password"
                    className=" rounded-r-lg flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                    placeholder="Contraseña"
                  />
                </div>
              </div>
              <div className="flex items-center mb-6 -mt-4">
                <div className="flex ml-auto">
                  <a
                    href="#"
                    className="inline-flex text-s font-thinner text-gray-500 sm:text-sm dark:text-gray-100 hover:text-gray-700 dark:hover:text-white"
                  >
                    Recordar contraseña
                  </a>
                </div>
              </div>
              <div className="flex w-full">
                <button
                  type="submit"
                  className="py-2 px-4  bg-purple-600 hover:bg-purple-700  text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg "
                  style={{
                    backgroundImage:
                      "linear-gradient(310deg, #2152ff 0%, #21d4fd 100%)",
                  }}
                >
                  Login
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
}
