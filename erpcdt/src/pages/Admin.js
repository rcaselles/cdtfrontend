import { useRouter } from "next/router";
import React, { Fragment } from "react";
import Taskbar from "../components/Taskbar";
import Announcements from "../components/Admin/Announcements";
import { useState } from "react";
import Inicio from "../components/Admin/Inicio";
export default function Admin({}) {
  const [actual, setActual] = useState("Inicio");
  function returnPage(val) {
    switch (val) {
      case "Anuncios":
        return <Announcements />;
      case "Inicio":
        return <Inicio />;
    }
  }
  return (
    <Fragment>
      <Taskbar
        title="Panel de Administración"
        hook={setActual}
        hookVal={actual}
      />

      {returnPage(actual)}
    </Fragment>
  );
}
