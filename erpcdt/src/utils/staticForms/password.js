const passwordform = [
  {
    name: "ChangePassword",
    form: [
      {
        columns: [
          {
            fields: [
              {
                type: "password",
                label: "Escribe tu contraseña vieja",
                default: "",
                name: "oldpassword",
              },
            ],
          },
          {
            fields: [
              {
                type: "password",
                label: "Escribe tu nueva contraseña",
                default: "",
                name: "newpassword",
              },
            ],
          },
          {
            fields: [
              {
                type: "password",
                label: "Confirma tu nueva contraseña",
                default: "",
                name: "newpassword2",
              },
            ],
          },
        ],
      },
    ],
  },
];
module.exports = passwordform;
