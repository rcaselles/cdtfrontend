export function findOccupied(date, reservations) {
  for (const element of reservations) {
    if (element.start_date <= date && element.end_date > date) {
      return { status: true, element: element };
    }
  }
  return { status: false };
}

export function dateList(fecha) {
  let current = new Date(getMonday(fecha));
  var week = [];
  for (var i = 0; i < 7; i++) {
    week.push(new Date(+current));
    current.setDate(current.getDate() + 1);
  }
  return week;
}

function getMonday(d) {
  d = new Date(d);
  var day = d.getDay(),
    diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}

//? rid: resource_id      sid: schedule_id */
export function goTo(router, rid, sid) {
  router.push({
    pathname: "/Calendar",
    query: {
      rid: rid,
      sid: sid,
      date: new Date().toISOString(),
    },
  });
}
