import { useState, useEffect, useContext } from "react";
import { getCookies } from "cookies-next";

import getLogged from "../services/logged/profile";
import getAnnouncements from "../services/announcements/announcements";
import getResources from "../services/resources/resources";
import CDTContext from "../../contexts/CDTContext";

const useDashboardData = () => {
  const token = getCookies("token").token;
  const { errores, setErrores } = useContext(CDTContext);
  const [logged, setLogged] = useState();
  const [announcement, setAnnouncement] = useState();
  const [resources, setResources] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getLogged(token).then(setLogged);
    getAnnouncements(token, "latest").then(setAnnouncement);
    getResources(undefined, token).then(setResources);
  }, [token]);

  useEffect(() => {
    if (logged && announcement && resources) {
      setLoading(false);
    }
  }, [announcement, logged, resources]);

  return {
    loggedInfo: logged,
    announcement,
    resource: resources,
    loading,
  };
};
export default useDashboardData;
