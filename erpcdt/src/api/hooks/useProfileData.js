import { useState, useEffect, useContext } from "react";
import { getCookies } from "cookies-next";

import getLogged from "../services/logged/profile";
import getAnnouncements from "../services/announcements/announcements";
import getResources from "../services/resources/resources";

const useProfileData = () => {
  const token = getCookies("token").token;
  const [logged, setLogged] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getLogged(token).then((profile) => {
      const email = profile[0].email;
      const username = profile[0].username;
      const fname = profile[0].fname;
      const lname = profile[0].lname;
      const phone = profile[0].phone;
      const organization = profile[0].organization;
      let profileform = [
        {
          name: "Profile",
          form: [
            {
              columns: [
                {
                  fields: [
                    {
                      type: "email",
                      label: "Correo electrónico",
                      default: email,
                      name: "email",
                    },
                    {
                      type: "text",
                      label: "Usuario",
                      default: username,
                      name: "username",
                    },
                  ],
                },
                {
                  fields: [
                    {
                      type: "text",
                      label: "Nombre",
                      default: fname,
                      name: "fname",
                    },
                    {
                      type: "text",
                      label: "Apellidos",
                      default: lname,
                      name: "lname",
                    },
                  ],
                },
                {
                  fields: [
                    {
                      type: "tel",
                      label: "Número de teléfono",
                      default: phone,
                      name: "phone",
                    },
                    {
                      type: "text",
                      label: "Compañía",
                      default: organization,
                      name: "company",
                    },
                  ],
                },
              ],
            },
          ],
        },
      ];
      setLogged(profileform[0].form[0].columns);
    });
  }, [token]);

  useEffect(() => {
    if (logged) {
      setLoading(false);
    }
  }, [logged]);

  return {
    loggedInfo: logged,
    loading,
  };
};
export default useProfileData;
