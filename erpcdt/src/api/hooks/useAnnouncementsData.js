import { useState, useEffect, useContext } from "react";
import { getCookies } from "cookies-next";
import getAnnouncements from "../services/announcements/announcements";

const useAnnouncementsData = () => {
  const token = getCookies("token").token;
  const [announcements, setAnnouncement] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getAnnouncements(token, "").then(setAnnouncement);
  }, [token]);

  useEffect(() => {
    if (announcements) {
      setLoading(false);
    }
  }, [announcements]);

  return {
    announcements,
    loading,
  };
};
export default useAnnouncementsData;
