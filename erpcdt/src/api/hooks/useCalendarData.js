import { useState, useEffect } from "react";
import { getCookies } from "cookies-next";
import { useRouter } from "next/router";

import getLogged from "../services/logged/profile";
import getLayouts from "../services/layouts/layouts";
import getReservations from "../services/reservations/reservations";
import getResources from "../services/resources/resources";

const useCalendarData = () => {
  const { query } = useRouter();
  const schedule_id = query.sid;
  const resource_id = query.rid;
  const date = query.date ? query.date : new Date().toISOString();
  const token = getCookies("token").token;
  const [logged, setLogged] = useState([]);
  const [reservations, setReservations] = useState([]);
  const [layout, setLayout] = useState([]);
  const [resources, setResources] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (query && query.sid && query.rid && date) {
      Promise.allSettled([
        getLogged(token),
        getReservations(resource_id, date, token),
        getLayouts(schedule_id, token),
        getResources(resource_id, token),
      ]).then(([logged, reservations, layout, resource]) => {
        const setCallback = (item, callback) => {
          if (item.status === "fulfilled") {
            callback(item.value);
          } else {
            console.error(item.reason);
          }
        };
        setCallback(logged, setLogged);
        setCallback(reservations, setReservations);
        setCallback(layout, setLayout);
        setCallback(resource, setResources);
      });
    }
  }, [query, resource_id, schedule_id, token]);

  useEffect(() => {
    if (logged && reservations && layout && resources) {
      setLoading(false);
    }
  }, [reservations, logged, layout, resources]);

  return {
    loggedInfo: logged,
    reservations,
    layout,
    resource: resources,
    loading,
  };
};
export default useCalendarData;
