import { URL } from "../../config";
export default async function getLogged(cookie) {
  const res = await fetch(URL + "users/logged", {
    method: "GET",
    headers: { "access-token": cookie },
  });
  return await res.json();
}
