import Swal from "sweetalert2";
import { getCookies } from "cookies-next";
export default function updatePassword(data) {
  if (data.newpassword !== data.newpassword2) {
    return Swal.fire({
      title: "Error",
      text: "La nueva contraseña no coincide",
      icon: "error",
      confirmButtonText: "Vale",
    });
  }
  fetch("http://localhost:1234/users/password", {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "access-token": getCookies("token").token,
    },
    method: "POST",
    body: JSON.stringify(data),
  })
    .then(function (res) {
      if (res.status == 200) {
        confirmDialog({
          message: "Tus datos han sido guardados con éxito",
          header: "Error",
          icon: "pi pi-exclamation-c",
          rejectLabel: " ",
          acceptLabel: "Ok",
        });
        Swal.fire({
          title: "Éxito",
          text: "Guardados correctamente",
          icon: "success",
          confirmButtonText: "Vale",
        });
      }
    })
    .catch(function (res) {
      console.log(res);
    });
}
