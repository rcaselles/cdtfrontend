import Swal from "sweetalert2";
import { getCookies } from "cookies-next";
import router from "next/router";
export default function updateProfile(data) {
  fetch("http://localhost:1234/users/update", {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "access-token": getCookies("token").token,
    },
    method: "POST",
    body: JSON.stringify(data),
  })
    .then(function (res) {
      if (res.status == 200) {
        Swal.fire({
          title: "Éxito",
          text: "Guardados correctamente",
          icon: "success",
          confirmButtonText: "Vale",
        });
      }
    })
    .catch(function (res) {
      console.log(res);
    });
}
