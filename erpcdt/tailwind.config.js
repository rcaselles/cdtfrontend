module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      margin: {
        "10%": "10%",
        "15%": "15%",
        "25%": "25%",
        "35%": "35%",
        "45%": "45%",
        "75%": "72.53%",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require("flowbite/plugin")],
  content: ["../node_modules/flowbite/**/*.js"],
};
